
#######
#  ~/.zshrc
#########################################################
####### UFP-Arch ZSH Master Configuration File    #######
#########################################################
#
# ______ _____ _    _                                                              
# |___  // ____| |  | |                                                             
#    / /| (___ | |__| |_ __ ___                                                     
#   / /  \___ \|  __  | '__/ __|                                                    
#  / /__ ____) | |  | | | | (__                                                     
# /_____|_____/|_|__|_|_|  \___|               ______       _____           _   _   
# |  _ \         |  _ \                       |  ____|     / ____|         | | | |  
# | |_) |_   _   | |_) |_ __ _   _  ___ ___   | |__       | (___   ___ ___ | |_| |_ 
# |  _ <| | | |  |  _ <| '__| | | |/ __/ _ \  |  __|       \___ \ / __/ _ \| __| __|
# | |_) | |_| |  | |_) | |  | |_| | (_|  __/  | |____ _    ____) | (_| (_) | |_| |_ 
# |____/ \__, |  |____/|_|   \__,_|\___\___|  |______(_)  |_____/ \___\___/ \__|\__|
#         __/ |                                                                     
#        |___/                                                         
#
#######################################################
#######   UFP-Arch v3 ZSH configuration file    #######
#### Has Partd of Anarchy ZSH configuration file ######
#######################################################
#
# This started out as "jonathan.zsh-theme" and "Anarchy ZSH configuration file"
# when on Feb. 23. 2020. I merged  the two files to add full functionallity
# because the original creator of "jonathan.zsh-theme" left some functions out
# that I wanted to use and I liked his Prompt and the way it worked.....
# The Anarchy ZSH Script had the other settings and functions that for the
# completion part of the script... --Bruce E. Scott

######################### 
### Set/unset ZSH options
#########################
# setopt NOHUP
# setopt NOTIFY
# setopt NO_FLOW_CONTROL

### Share commands between terminal instances or not
####################################################
# unsetopt SHARE_HISTORY
setopt INC_APPEND_HISTORY SHARE_HISTORY
setopt APPEND_HISTORY
# setopt AUTO_LIST
# setopt AUTO_REMOVE_SLASH
# setopt AUTO_RESUME
unsetopt BG_NICE
setopt CORRECT
setopt EXTENDED_HISTORY
# setopt HASH_CMDS
setopt MENUCOMPLETE
#setopt ALL_EXPORT

### Set/unset  shell options
############################
setopt   notify globdots correct pushdtohome cdablevars autolist
# setopt   GLOB_DOTS
setopt   correctall autocd recexact longlistjobs
setopt   autoresume histignoredups pushdsilent 
setopt   autopushd pushdminus extendedglob rcquotes mailwarning
unsetopt bgnice autoparamslash

### Autoload zsh modules when they are referenced
#################################################
autoload -U history-search-end
zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof
#zmodload -ap zsh/mapfile mapfile
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end

### Set variables
#################
## set PATH so it includes user's private bin directories
PATH="$HOME/bin:$HOME/.local/bin:$PATH"
PATH="/usr/local/bin:/usr/local/sbin/:$PATH"

if [ -d "$HOME/.bin" ] ;
  then PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ;
  then PATH="$HOME/.local/bin:$PATH"
fi
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

HISTFILE=$HOME/.zhistory
HISTSIZE=2000
SAVEHIST=2000
# HOSTNAME="`hostname`"
LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

### Path to your oh-my-zsh installation.
#######################################
export ZSH="/home/bruce/.oh-my-zsh"

### Compilation flags
#####################
 export ARCHFLAGS="-arch x86_64"

### Load Themes
###############
# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "johnathan" "repos" "rkj-repos" "xiong-chiamiov" )

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
# ZSH_THEME="robbyrussell"
# ZSH_THEME="random"
# ZSH_THEME="powerlevel10k/powerlevel10k" #<-- Custon version
ZSH_THEME="powerlevel10k/powerlevel10k-2" #<-- stock version
# ZSH_THEME="powerlevel10k/powerlevel10k-3"
# ZSH_THEME="ufp" #Not Used
# ZSH_THEME="ufp-V2" #Not Used
# ZSH_THEME="ufp-V3"
# This "jonathan.zsh-theme" is what was used to make the UFP-v2 theme.
# ZSH_THEME="jonathan"
# ZSH_THEME="agnoster"
# ZSH_THEME="josh"

### source $ZSH/oh-my-zsh.sh
############################
if [ -f $ZSH/oh-my-zsh.sh ]; then
  source $ZSH/oh-my-zsh.sh
fi

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "johnathan" "repos" "rkj-repos" "xiong-chiamiov" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
 DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction. <<<---- bes
 ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
 COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

### Load colors
###############
autoload colors zsh/terminfo
if [[ "$terminfo[colors]" -ge 8 ]]; then
   colors
fi
for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
   eval PR_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
   eval PR_LIGHT_$color='%{$fg[${(L)color}]%}'
   (( count = $count + 1 ))
done

### Set Colors to use in in the script
#############
# Normal Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Purple='\e[0;35m'       # Purple
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BPurple='\e[1;35m'      # Purple
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Purple='\e[45m'      # Purple
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

NC="\e[m"               # Color Reset

### Bind keys
#############
autoload -U compinit
compinit
bindkey "^?" backward-delete-char
bindkey '^[OH' beginning-of-line
bindkey '^[OF' end-of-line
bindkey '^[[5~' up-line-or-history
bindkey '^[[6~' down-line-or-history
bindkey "^[[A" history-beginning-search-backward-end
bindkey "^[[B" history-beginning-search-forward-end
bindkey "^r" history-incremental-search-backward
bindkey ' ' magic-space    # also do history expansion on space
bindkey '^I' complete-word # complete on tab, leave expansion to _expand

### Zstyle Completion Beginning
###############################
zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path ~/.zsh/cache/$HOST
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' menu select=1 _complete _ignored _approximate
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'

### Completion Styles
#####################

# list of completers to use
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate

# allow one error for every three characters typed in approximate completer
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'
    
# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order all-expansions

# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# command for process lists, the local web server details and host completion
# on processes completion complete all user processes
zstyle ':completion:*:processes' command 'ps -au$USER'

## add colors to processes for kill completion
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

#zstyle ':completion:*:processes' command 'ps -o pid,s,nice,stime,args'
#zstyle ':completion:*:urls' local 'www' '/var/www/htdocs' 'public_html'
#
#NEW completion:
# 1. All /etc/hosts hostnames are in autocomplete
# 2. If you have a comment in /etc/hosts like #%foobar.domain,
#    then foobar.domain will show up in autocomplete!
zstyle ':completion:*' hosts $(awk '/^[^#]/ {print $2 $3" "$4" "$5}' /etc/hosts | grep -v ip6- && grep "^#%" /etc/hosts | awk -F% '{print $2}') 
# Filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' \
    '*?.old' '*?.pro'
# the same for old style completion
#fignore=(.o .c~ .old .pro)

# ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion:*:*:*:users' ignored-patterns \
        adm apache bin daemon games gdm halt ident junkbust lp mail mailnull \
        named news nfsnobody nobody nscd ntp operator pcap postgres radvd \
        rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs avahi-autoipd\
        avahi backup messagebus beagleindex debian-tor dhcp dnsmasq fetchmail\
        firebird gnats haldaemon hplip irc klog list man cupsys postfix\
        proxy syslog www-data mldonkey sys snort
# SSH Completion
zstyle ':completion:*:scp:*' tag-order \
   files users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:scp:*' group-order \
   files all-files users hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:ssh:*' tag-order \
   users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:ssh:*' group-order \
   hosts-domain hosts-host users hosts-ipaddr
zstyle '*' single-ignored show

## Zstyle Completion Ending
######################

### ------------------------------------------------------------------------------ ###

### User configuration
######################

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

### Unset Some OPT's
###############
#unsetopt ALL_EXPORT

### Preferred editor for local and remote sessions
 if [[ -n $SSH_CONNECTION ]]; then
   export EDITOR='nano'
 else
   export EDITOR='nano'
 fi

# export PAGER='most'
# export EDITOR='nano'
# export VISUAL='nano'

# Setting the z.sh database to the ~/home/bin
# set $_Z_DATA $HOME/.local <-- Not Working

### set common functions
#############

### Set Aliases -->> See ~/.bashrc_aliases & ~/.Persomal
######################################
# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

 if [ -f ~/.zsh_aliases ]; then
   . ~/.zsh_aliases
 fi

 if [ -f ~/.bash_aliases ];then
    . ~/.bash_aliases
 fi

### Source plugins / Source Files
#################################
# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# plugins=(git)
plugins=(z git-prompt)
# plugins=(you-should-use $plugins)

source ~/.oh-my-zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source ~/.oh-my-zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
# source ~/.config/broot/launcher/bash/br 2>/dev/null

 if [ -f /home/bruce/.config/broot/launcher/bash/br ]; then
    source /home/bruce/.config/broot/launcher/bash/br 2>/dev/null
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Setting the z.sh database to the ~/home/bin
# set $_Z_DATA $HOME/.local <-- Not Working
# Z  Tracks your most used directories.
. ~/bin/work/z.sh

## Additions
##################

### Sourceing Custom Fetch Screens
##################################

# Run /bin/end-info-v3-icons info screen when starting bash
 if [ -f ~/bin/end-info-v3-icons ]; then
    . ~/bin/end-info-v3-icons

# Run /bin/end-info-v3-icons info screen when starting bash
 elif [ -f ~/bin/end-info-v3-icons-obs ]; then
    . ~/bin/end-info-v3-icons-obs

# Run /bin/end-info-v3 info screen when starting bash
 elif [ -f ~/bin/end-info-v3 ]; then
    . ~/bin/end-info-v3

# Run /bin/Archinfo-v3-bes info screen when starting bash
 elif [ -f ~/bin/archinfo-v3-bes ]; then
    . ~/bin/archinfo-v3-bes

# Run /bin/manjarinfo info screen when starting bash
 elif [ -f ~/bin/manjarinfo ]; then
    . ~/bin/manjarinfo

# Run /bin/archinfo-v4-bes-icons info screen when starting bash
 elif [ -f ~/bin/archinfo-v4-bes-icons ]; then
    . ~/bin/archinfo-v4-bes-icons

  else
    neofetch

fi

# ------------------------------------------------
### Getting Current Weather & Colors for STD output... 07/15/2021 -Bruce
######################################################
# ------------------------------------------------

# ~/bin/archinfo-v3-bes
# echo -e Current Weather
# ~/bin/weather.sh

### Color Library
#################
default=`echo -en "\e[39m"`
red=`echo -en "\e[31m"`
orange=`echo -en "\e[33m"`
blue=`echo -en "\e[34m"`
gray=`echo -en "\e[30m"`

bold=`echo -en "\e[1m"`
normal=`echo -en "\e[0m"`

# echo -e Current Weather
# Run /bin/Kweather.sh to print Weather om screen when starting Bash/Zsh
 if [ -f /home/bruce/Documents/Master-Docs/term-weather.txt ]; then
#    . ~/bin/archinfo-v3-bes
    cat /home/bruce/Documents/Master-Docs/term-weather.txt

  else
    echo -e "\t --> ${red}ERROR ${orange}term-weather.txt File Not Found! ${red}ERROR ${default}<--${default}\n"
    echo -e "\t -> Possiable Fix's"
    echo -e "\t -> 1. Check Server CronJob or Path or term-weather.txt"
    echo -e "\t -> 2. Check path or file in /N-1-MD-500-Sata/Master-Documents/term-weather.txt"

fi
