# 
# Name: Titus-2 Conky
#
# Author: Bruce E. Scott 12/6/2019.
#
# Description
#
# Apart from the requirement for the Ubuntu font, this configuration is
# really simple, using only a few conky objects and a script provided
# with the configuration file.
#
# Installation (see below for terminal installation)
# To install and use, the following 5 main steps are required:
# 1. Install conky and the Ubuntu font (if using recent Ubuntu (based) 
#    distributions, this step is most certainly not necessary.
# 2. Create a directory named .conky in your home folder;
# 3. Download the .zip file and unzip the contents to the newly 
#    created folder.
#
# If you want conky to startup with gnome, copy the file named
# conky.sh.desktop to ~.config/autostart/
# If you do not want a given module to run (e.g. the GMAIL module),
# edit the conky.sh file and comment or remove the line that loads the
# respective configuration file.
#
# To install using the terminal, follow the next steps
# 1. In Ubuntu (I have not tested the config in Ubuntu, only in Arch)
#    sudo apt-get install conky-all
#    In Arch Linux
#    sudo yaourt -S conky-lua ttf-ubuntu-font-family 
# 2. mkdir ~/.conky && cd ~/.conky
# 3. wget http://www.deviantart.com/download/260230653/metro_style_conky_by_kant_o-d4axn99.zip
# 4. unzip MetroStyleConky.zip
# 5. chmod +x conky.sh
# 6. chmod +x mail.sh
# 7. ./conky.sh   
# 8. (optional) run gnome-session-properties and add a new 
#    startup program pointing to conky.sh to have the conky
#    automatically comming up when you start the computer. 
#

alignment top_left
background yes
border_width 1
cpu_avg_samples 1
default_color ffffff
draw_outline no
draw_borders yes
draw_shades no
use_xft yes
# xftfont Ubuntu:size=12
xftfont Bitstream Vera Sans Mono:size=7
# size of text area
minimum_size 340 120
maximum_width 360
max_text_width 3600

no_buffers yes
out_to_console no
out_to_stderr no
extra_newline no
own_window yes
own_window_transparent yes
own_window_class Conky
own_window_type normal
own_window_hints undecorated,sticky,skip_taskbar,skip_pager,below
stippled_borders 0
update_interval 1.0
uppercase no
use_spacer none
show_graph_scale no
show_graph_range no

# Use double buffering (reduces flicker, may not work for everyone)
double_buffer yes

own_window_colour 136659
update_interval 5

gap_x 35
gap_y 10
own_window_argb_value 0
own_window_argb_visual no

#  --Colours

default_color D9DDE2  # -- (white-gray) default color and border color
color1 FF0000 # --red
color2 597AA1 # --blue-gray
color3 cccccc # --light-gray
color4 D9BC83 # --shade-of-gold
color5 00BFFF # --Mid-light-blue_(gray-blue)
color6 FFFFFF # --white

#	--Signal Colours
color7 1F7411 # --green
color8 FFA726 # --orange
color9 F1544B # --firebrick


TEXT
${color9}${voffset 4}${font GE Inspira:size=20}${alignc}${time %l}:${time %M} ${time %p}${font}${color}
${color7}${voffset 4}${font GE Inspira:size=12}${alignc}${time %A} ${time %B} ${time %e}, ${time %Y}${font}${color}

${color5}${font Roboto:size=7}${voffset 2}S Y S T E M   ${hr 2}${font}${color}
${color2}${voffset 8}Hostname:${color} ${alignr}${nodename}
${color2}Distro:${color}${alignr}$sysname $kernel ${alignr}${execi 6000 lsb_release -a | grep 'Release'|awk {'print $2""$3""$4""$5'}}
${color2}Kernel:${color}${alignr}${exec uname} ${exec uname -r}
${color2}Linux: ${color}${alignr}${execi 1000 cat /etc/os-release | grep 'PRETTY_NAME' | sed -e 's/PRETTY_NAME.//'}
${color2}On Build: ${color}${alignr}${execi 1000 cat /etc/os-release | grep 'ID_LIKE' | sed -e 's/ID_LIKE.//'}
#
#Nvidia: ${alignr}${execp  nvidia-smi --query-supported-clocks=gpu_name --format=csv,noheader}
#Nvidia Driver: ${alignr}${execi 60000 nvidia-smi | grep "Driver Version"| awk {'print $3'}}
#
${color2}Uptime:${color} ${alignr}${uptime}
#
${color5}${font Roboto:size=7}P R O C E S S O R S  ${hr 2}${font}${color}
${color2}CPU Mfg: ${alignc}${color FF8700}${execi 1000 lscpu | grep Model\ name | sed 's/name:\ //' | cut -c 27- | sed "s/([^)]*)/()/g" | sed 's/[)(]//g'}
${color2}CPU Freq:${color} $alignr${offset -30}${freq}MHz
# ${color2}CPU Temp:${color} $alignr${execi 10 sensors | grep 'Core 0' | awk {'print $3'}}
${color2}History:${color} ${alignr}${cpugraph 8,100}
${color2}${offset 30}CPU Core 1:${color} ${alignr}${offset -30}${cpu cpu1}%${alignr}${cpubar cpu1 8,100}
${color2}${offset 30}CPU Core 2:${color} ${alignr}${offset -30}${cpu cpu2}%${alignr}${cpubar cpu2 8,100}
${color2}${offset 30}CPU Core 3:${color} ${alignr}${offset -30}${cpu cpu3}%${alignr}${cpubar cpu3 8,100}
${color2}${offset 30}CPU Core 4:${color} ${alignr}${offset -30}${cpu cpu4}%${alignr}${cpubar cpu4 8,100}
${color2}${offset 30}CPU Core 5:${color} ${alignr}${offset -30}${cpu cpu5}%${alignr}${cpubar cpu5 8,100}
${color2}${offset 30}CPU Core 6:${color} ${alignr}${offset -30}${cpu cpu6}%${alignr}${cpubar cpu6 8,100}
#
${color2}Top Processes${goto 170}PID${goto 228}cpu%${goto 288}mem%${color}
${voffset 4}     ${color}1  -  ${color9}${top name 1}${alignr} ${goto 168}${top pid 1} ${goto 170} ${goto 222}${top cpu 1} ${goto 274}${top mem 1}
     ${color}2  -  ${color8}${top name 2}${alignr} ${goto 168}${top pid 2} ${goto 222}${top cpu 2} ${goto 274}${top mem 2}
     ${color}3  -  ${color7}${top name 3}${alignr} ${goto 168}${top pid 3} ${goto 222}${top cpu 3} ${goto 274}${top mem 3}
     ${color}4  -  ${top name 4}${alignr} ${goto 168}${top pid 4} ${goto 222}${top cpu 4} ${goto 274}${top mem 4}
     ${color}5  -  ${top name 5}${alignr} ${goto 168}${top pid 5} ${goto 222}${top cpu 5} ${goto 274}${top mem 5}
#
${color5}${font Roboto:size=7}M E M O R Y   ${hr 2}${font}${color}
${color2}${offset 30}RAM: ${color}${alignr}${offset -10}${mem} / ${memmax}${alignr}${membar 8,100}
${color2}${offset 30}Swap:${color} ${alignr}${offset -10}${swap} / ${swapmax}${alignr}${swapbar 8,100}
#
${color2}Top Processes ${goto 170}PID${goto 228}cpu%${goto 288}mem%${color}
${voffset 4}     ${color}1  -  ${color9}${top_mem name 1}${alignr}${goto 168}${top_mem pid 1} ${goto 170} ${goto 222}${top_mem cpu 1} ${goto 274}${top_mem mem 1}
     ${color}2  -  ${color8}${top_mem name 2}${alignr}${goto 168}${top_mem pid 2} ${goto 170} ${goto 222}${top_mem cpu 2} ${goto 274}${top_mem mem 2}
     ${color}3  -  ${color7}${top_mem name 3}${alignr}${goto 168}${top_mem pid 3} ${goto 170} ${goto 222}${top_mem cpu 3} ${goto 274}${top_mem mem 3}
     ${color}4  -  ${top_mem name 4}${alignr}${goto 168}${top_mem pid 4} ${goto 170} ${goto 222}${top_mem cpu 4} ${goto 274}${top_mem mem 4}
     ${color}5  -  ${top_mem name 5}${alignr}${goto 168}${top_mem pid 5} ${goto 170} ${goto 222}${top_mem cpu 5} ${goto 274}${top_mem mem 5}
#
${color5}${font Roboto:size=7}D R I V E   S P A C E   ${hr 2}${font}${color}
${offset 30}${color2}Root HDD :${color} ${alignr}${offset -10}${fs_used /} / ${fs_size /}${alignr}${fs_bar 8,100}
${offset 30}${color2}I/O Read:${color} ${alignr}${offset -10}${diskio_read /dev/sda1}${alignr}${diskiograph_read /dev/sda1 8,100}
${offset 30}${color2}I/O Write:${color} ${alignr}${offset -10}${diskio_write /dev/sda1}${alignr}${diskiograph_write /dev/sda1 8,100}

${offset 30}${color2}Boot :${color} ${alignr}${offset -10}${fs_used /boot} / ${fs_size /boot}${alignr}${fs_bar 8,100}
${offset 30}${color2}I/O Read:${color} ${alignr}${offset -10}${diskio_read /dev/sda3}${alignr}${diskiograph_read sda3 8,100}
${offset 30}${color2}I/O Write:${color} ${alignr}${offset -10}${diskio_write /dev/sda3}${alignr}${diskiograph_write sda3 8,100}

${offset 30}${color2}Bruce :${color} ${alignr}${offset -10}${fs_used /home/bruce} / ${fs_size /home/bruce}${alignr}${fs_bar 8,100}
${offset 30}${color2}I/O Read:${color} ${alignr}${offset -10}${diskio_read /dev/sda2}${alignr}${diskiograph_read /dev/sda2 8,100}
${offset 30}${color2}I/O Write:${color} ${alignr}${offset -10}${diskio_write /dev/sda2}${alignr}${diskiograph_write /dev/sda2 8,100}
#
${color5}${color5}${font Roboto:size=7}N E T W O R K   ${hr 2}${font}${color}
${color2}${offset 30}IP Address: ${color} ${alignr}${offset -10$}${addrs enp3s0}
${color2}${offset 30}Eth Up:${color} ${alignr}${offset -10$}${upspeed enp3s0}${alignr}${upspeedgraph enp3s0 8,100}
${color2}${offset 30}Eth Down:${color} ${alignr}${offset -10$}${downspeed enp3s0}${alignr}${downspeedgraph enp3s0 8,100}
${color2}${offset 30}Total Dl:${color white}${totaldown enp3s0} ${alignr}${offset -30}${color2}Total UL:${alignr}${offset -30}${color white}${totalup enp3s0}
#
#
#${font Roboto:size=10}N V I D I A   ${hr 2}${font}
#${font Roboto:size=10,weight:bold}${color5}${execp  nvidia-smi --query-supported-clocks=gpu_name --format=csv,noheader}${font}
#${font StyleBats:size=20}u${font}${offset 8}${voffset -12}GPU Temp ${alignr}${execi 60 nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader} °C
#${offset 30}Fan Speed ${alignr}${execi 60 nvidia-settings -q [fan:0]/GPUCurrentFanSpeed -t} %
#${offset 30}GPU Clock ${alignr}${execi 60 nvidia-settings -q GPUCurrentClockFreqs | grep -m 1 Attribute | awk '{print $4}' | sed -e 's/\.//' | cut -d, -f1} MHz
#${offset 30}Mem Clock ${alignr}${execi 86400 nvidia-settings -q all| grep -m 1 GPUCurrentProcessorClockFreqs | awk '{print $4}' | sed 's/.$//'} MHz
#${offset 30}Mem Used ${alignr}${execi 60 nvidia-settings -q [gpu:0]/UsedDedicatedGPUMemory -t} / ${exec nvidia-settings -q [gpu:0]/TotalDedicatedGPUMemory -t} MiB0
