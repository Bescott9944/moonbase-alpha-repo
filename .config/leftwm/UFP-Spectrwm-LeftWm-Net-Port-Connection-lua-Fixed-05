--[[
#=====================================================================================
#
# Date    : package-date
# name    : syclo conky (Simple System Info And Clock)
# Author  : Adhi Pambudi
#           https://plus.google.com/+AdhiPambudi
#           http://addy-dclxvi.deviantart.com/
# Github  : https://github.com/zagortenay333/conky-Vision
# Editor  : Erik Dubois
# Version : package-version
# License : Distributed under the terms of GNU GPL version 2 or later
#=====================================================================================
# CONKY
# For commands in conky.config section:
# http://conky.sourceforge.net/config_settings.html
#
# For commands in conky.text section:
# http://conky.sourceforge.net/variables.html
#
# A PDF with all variables is provided
#=====================================================================================
# FONTS
# To avoid copyright infringements you will have to download
# and install the fonts yourself sometimes.
#=====================================================================================
# GENERAL INFO ABOUT FONTS
# Go and look for a nice font on sites like http://www.dafont.com/
# Download and unzip - double click the font to install it (font-manager must be installed)
# No font-manager then put fonts in ~/.fonts
# Change the font name in the conky 
# The name can be known with a command in the terminal: fc-list | grep "part of name"
# Change width and height of the conky according to font
# Reboot your system or fc-cache -fv in terminal
# Enjoy
#=====================================================================================
# FONTS FOR THIS CONKY
# no extra font(s) needed
#=====================================================================================
# The conky code is not in the lua format.

]]


conky.config = {

--##  Begin Window Settings  #####################

own_window = true,
own_window_type = 'desktop',
own_window_transparent = true,
own_window_hints = 'undecorated,below,skip_taskbar,skip_pager,sticky',
own_window_colour = '#000000',
own_window_class = 'Conky',
own_window_title = 'arcolinux  Ccnky',
--## ARGB can be used for real transparency
--## NOTE that a composite manager is required for real transparency.
--## This option will not work as desired (in most cases) in conjunction with
--## own_window_type normal
-- own_window_argb_visual yes # Options: yes or no

--## When ARGB visuals are enabled, this use this to modify the alpha value
--## Use: own_window_type normal
--## Use: own_window_transparent no
--## Valid range is 0-255, where 0 is 0% opacity, and 255 is 100% opacity.
-- own_window_argb_value 50

	--Windows

--.	own_window = true,							-- create your own window to draw
--	own_window_argb_value = 100,			    -- real transparency - composite manager required 0-255
--	own_window_argb_visual = true,				-- use ARGB - composite manager required
--.	own_window_colour = '000000',				-- set colour if own_window_transparent no
--.	own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',  -- if own_window true - just hints - own_window_type sets it
--.	own_window_transparent = false,				-- if own_window_argb_visual is true sets background opacity 0%
--.	own_window_title = 'system_conky',			-- set the name manually  - default conky "hostname"
--.	own_window_type = 'normal',				-- if own_window true options are: normal/override/dock/desktop/panel


--Placement

alignment = 'top_right',                      	-- top_left,top_middle,top_right,bottom_left,bottom_middle,bottom_right,
							                	-- middle_left,middle_middle,middle_right,none
    gap_x = 25,--## left | right
    gap_y = 60,--## up | down
--	gap_x = 435,								-- pixels between right or left border
--	gap_y = 60,									-- pixels between bottom or left border
	--minimum_height = 600,						-- minimum height of window
	--minimum_width = 300,						-- minimum height of window
	--maximum_width = 300,						-- maximum height of window

    minimum_width = 740,
    minimum_height = 220,
	maximum_width = 760,
	max_text_width = 3600,

--######################  End Window Settings  ###

--##  Font Settings  #############################

     --Textual

	extra_newline = false,						-- extra newline at the end - for asesome's wiboxes
	format_human_readable = true,				-- KiB, MiB rather then number of bytes
--	font = 'Roboto Mono:size=09',  				-- font for complete conky unless in code defined
--	max_text_width = 0,							-- 0 will make sure line does not get broken if width too smal
--	max_user_text = 16384,						-- max text in conky _ 16384
	override_utf8_locale = true,				-- force UTF8 requires xft
--	short_units = true,							-- shorten units from KiB to k
--	top_name_width = 21,						-- width for $top name value default 15
--	top_name_verbose = false,					-- If true, top name shows the full command line of  each  process - Default value is false.
	uppercase = false,							-- uppercase or not
--	use_spacer = 'none',						-- adds spaces around certain objects to align - default none
	use_xft = true,								-- xft font - anti-aliased font
	xftalpha = 1,								-- alpha of the xft font - between 0-1
    font = 'Roboto:size=9.5',
--########################  End Font Settings  ###

--##  Colour Settings  ###########################
draw_shades = false,--yes
default_shade_color = 'black',

draw_outline = false,-- amplifies text if yes
default_outline_color = 'black',


	--Colours

    --default_color = '#D9DDE2',  				-- default color and border color
    -- default_color #D9DDE2     --(white-gray) default color and border color
	default_color = '#00BFFF',  --Mid-light-blue_(gray-blue) default color and border color

	color00 = '#D9DDE2',          --(white-gray)
	color1 = '#FF0000',          --Pure (or mostly pure) red
	color2 = '#597AA1',          --Mostly desaturated dark blue
	color3 = '#cccccc',          --Light gray
	color4 = '#D9BC83',          --Very soft orange
--	color5 = '#00BFFF',          --Pure (or mostly pure) blue / -Mid-light-blue_(gray-blue)
	color6 = '#FFFFFF',          --Pure White
	--Signal Colours
	color7 = '#1F7411',       	--Dark lime green
	color8 = '#FFA726',       	--Vivid orange
--	color9 = '#F1544B',       	--firebrick/Soft red

    -- # ArcoLinux Colors #
    --default_color 656667 # Waldorf original colour
    --default_color 7a7a7a # Flame  & Bunsen Grey
    --default_color 929292 # Labs Grey
    default_color = '#ffffff',-- PureWhite
--    color0 = '#DC143C',        -- Crimson
--    color1 = '#778899',        -- LightSlateGray
--    color2 = '#D8BFD8',        -- Thistle
--    color3 = '#9ACD32',        -- YellowGreen
--    color4 = '#FFA07A',        -- LightSalmon
    color5 = '#FFDEAD',        -- NavajoWhite
--    color6 = '#00BFFF',        -- DeepSkyBlue
--    color7 = '#5F9EA0',        -- CadetBlue
--    color8 = '#BDB76B',        -- DarkKhaki
    color9 = '#CD5C5C',        -- IndianRed

--######################  End Colour Settings  ###

--##  Borders Section  ###########################

--ARCOLINUX >>
-- >>draw_borders = false,
-- Stippled borders?
-- >>stippled_borders = 5,
-- border margins
-- >>border_inner_margin = 5,
-- >>border_outer_margin = 0,
-- border width
-- >>border_width = 2,
-- graph borders
--> draw_graph_borders = true,--no
--default_graph_size 15 40

	--Graphical

	border_inner_margin = 10, 					-- margin between border and text
	border_outer_margin = 5, 					-- margin between border and edge of window
	border_width = 1, 							-- border width in pixels
	default_bar_width = 80,		    			-- default is 0 - full width
	default_bar_height = 10,					-- default is 6
	default_gauge_height = 25,					-- default is 25
	default_gauge_width =40,					-- default is 40
	default_graph_height = 40,					-- default is 25
	default_graph_width = 0,					-- default is 0 - full width
	default_shade_color = '#000000',			-- default shading colour
	default_outline_color = '#000000',			-- default outline colour
	draw_borders = true,						-- draw borders around text
	draw_graph_borders = true,					-- draw borders around graphs
	draw_borders = false,						-- draw borders around text
	draw_shades = false,						-- draw shades
	draw_outline = false,						-- draw outline
	stippled_borders = 0,						-- dashing the border

--#######################  End Borders Secton  ###


--##  Miscellaneous Section  #####################

-- Boolean value, if true, Conky will be forked to background when started.
background = true,
-- Adds spaces around certain objects to stop them from moving other things
-- around, this only helps if you are using a mono font
-- Options: right, left or none
use_spacer = 'none',

-- Default and Minimum size is 256 - needs more for single commands that
-- "call" a lot of text IE: bash scripts
--text_buffer_size 6144

-- Subtract (file system) buffers from used memory?
no_buffers = true,

-- change GiB to G and MiB to M
short_units = true,

-- Like it says, ot pads the decimals on % values
-- doesn't seem to work since v1.7.1
pad_percents = 2,

-- Imlib2 image cache size, in bytes. Default 4MiB Increase this value if you use
-- $image lots. Set to 0 to disable the image cache.
imlib_cache_size = 0,

-- Use the Xdbe extension? (eliminates flicker)
-- It is highly recommended to use own window with this one
-- so double buffer won't be so big.
double_buffer = true,

--   Maximum size of user text buffer, i.e. layout below TEXT line in config file
--  (default is 16384 bytes)
-- max_user_text 16384

-- Desired output unit of all objects displaying a temperature. Parameters are
-- either "fahrenheit" or "celsius". The default unit is degree Celsius.
-- temperature_unit Fahrenheit
--################  End Miscellaneous Section  ###

update_interval = 2,

};


conky.text = [[
${color8}${font Roboto:size=9.5}${voffset 2}O P E N   C O N N E C T I O N  ${alignr}${color8}${font Roboto:size=9.5}${voffset 2}${offset -70}P O R T S   C O N N E C T I O N ${font}
${color5}${hr 2}
${alignc}${color9}${offset -40}Open Ports : ${color6}${tcp_portmon 1 65535 count}
# ${alignr}${offset -30}${color5}${font Roboto:size=9.5}${voffset 2}P O R T S
${color5}${hr 2}
# ${color yellow}Open Connection ${hr 2}
${color8}Inbound Connection ${alignc}Local Service/Port ${alignr}${color8}${offset -110}IP Address${alignr}${offset -10}${color8}DPORT
 ${color white}${tcp_portmon 1 32767 rhost 0}${alignc}${tcp_portmon 1 32767 lservice 0} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  0}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  0}
 ${tcp_portmon 1 32767 rhost 1} ${alignc}${tcp_portmon 1 32767 lservice 1} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  1}${alignr 1}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  1}
 ${tcp_portmon 1 32767 rhost 2} ${alignc}${tcp_portmon 1 32767 lservice 2} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  2}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  2}
 ${tcp_portmon 1 32767 rhost 3} ${alignc}${tcp_portmon 1 32767 lservice 3} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  3}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  3}
 ${tcp_portmon 1 32767 rhost 4} ${alignc}${tcp_portmon 1 32767 lservice 4} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  4}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  4}
 ${tcp_portmon 1 32767 rhost 5} ${alignc}${tcp_portmon 1 32767 lservice 5} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  5}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  5}
 ${tcp_portmon 1 32767 rhost 6} ${alignc}${tcp_portmon 1 32767 lservice 6} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  6}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  6}
 ${tcp_portmon 1 32767 rhost 7} ${alignc}${tcp_portmon 1 32767 lservice 7} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  7}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  7}
 ${tcp_portmon 1 32767 rhost 8} ${alignc}${tcp_portmon 1 32767 lservice 8} ${alignr}${offset -110}${color6}${tcp_portmon 1 65535 rip  8}${alignr 1}${offset -11}${tcp_portmon 1 65535 rport  8}
${color8}Outbound Connection ${alignc}${offset 40}Remote Service/Port${color white}${alignr}${offset -110}${color8}${font Roboto:size=9.5}${voffset 2}S Y S T E M    U P D A T E S  ${color5}${hr 2}${font}${color white}
 ${tcp_portmon 32768 61000 rhost 0} ${alignc}${offset 30}${tcp_portmon 32768 61000 rservice 0} ${alignr}${offset -80}${color6} My Linux: Arch $alignr${color8}${execi 10000 awk -F'=' '/DISTRIB_ID=/ {printf $2" "} /CODENAME/ {print $2}' /etc/lsb-release}
 ${color white}${tcp_portmon 32768 61000 rhost 1} ${alignc}${offset -60}${tcp_portmon 32768 61000 rservice 1} ${alignr}${offset -230}${color5}${hr 2}
 ${color white}${tcp_portmon 32768 61000 rhost 2} ${alignc}${offset 03}${tcp_portmon 32768 61000 rservice 2} ${alignr}${offset -130}${color6} Available Updates: ${alignr}${color9}${font Cantarell:bold:size=12}${execpi 1000 checkupdates | wc -l}${font}${color white}
 ${tcp_portmon 32768 61000 rhost 3} ${alignc}${offset 44}${tcp_portmon 32768 61000 rservice 3} ${alignr}${offset -58}${color6} Last Updated On: ${alignr}${color8}${execi 10000 grep "starting full system upgrade" /var/log/pacman.log | tail -n1| cut --bytes=2-17}${color white}
 ${tcp_portmon 32768 61000 rhost 4} ${alignc}${offset -60}${tcp_portmon 32768 61000 rservice 4}
 ${tcp_portmon 32768 61000 rhost 5} ${alignc}${offset -60}${tcp_portmon 32768 61000 rservice 5}
 ${tcp_portmon 32768 61000 rhost 6} ${alignc}${offset -60}${tcp_portmon 32768 61000 rservice 6}
 ${tcp_portmon 32768 61000 rhost 7} ${alignc}${offset -60}${tcp_portmon 32768 61000 rservice 7}

]];
