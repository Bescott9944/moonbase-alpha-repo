;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
background = #2e3440
background-alt = #444
foreground = #d8dee9
foreground-alt = #888
primary = #5e81ec
secondary = #e60053
alert = #bd2c40

[bar/mainbar0]
inherit = bar/barbase
modules-left = workspace0
[module/workspace0]
type = custom/script
exec = leftwm-state -w 0 -t $SCRIPTPATH/template.liquid
tail = true

[bar/mainbar1]
inherit = bar/barbase
modules-left = workspace1
[module/workspace1]
type = custom/script
exec = leftwm-state -w 1 -t $SCRIPTPATH/template.liquid
tail = true

[bar/mainbar2]
inherit = bar/barbase
modules-left = workspace2
[module/workspace2]
type = custom/script
exec = leftwm-state -w 2 -t $SCRIPTPATH/template.liquid
tail = true

[bar/mainbar3]
inherit = bar/barbase
modules-left = workspace3
[module/workspace3]
type = custom/script
exec = leftwm-state -w 3 -t $SCRIPTPATH/template.liquid
tail = true

[bar/barbase]
width = ${env:width}
offset-x = ${env:offsetx}
monitor = ${env:monitor}
;offset-y = ${env:y}
;width = 100%
height = 28
fixed-center = false
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 3
line-color = #f00
border-size = 0
border-color = #00000000
padding-left = 0
padding-right = 4
module-margin-left = 1
module-margin-right = 0.95
font-0 = "UbuntuMono Nerd Font:size=12;1"
font-1 = "UbuntuMono Nerd Font:size=14;1"
font-2 = "Font Awesome 5 Free:style=Regular:pixelsize=12;1"
font-3 = "Font Awesome 5 Free:style=Solid:pixelsize=12;1"
font-4 = "Font Awesome 5 Brands:pixelsize=12;1"
font-5 = "NotoColorEmoji:scale=12:pixelsize=12;1"
modules-center = date2
modules-right = networkspeeddownbes sept networkspeedupbes sept memory sept cpubes2 sept temps sept network-localip sept updates
;network date volume
tray-position = right
tray-padding = 1
cursor-click = pointer
cursor-scroll = ns-resize
bottom = true
;[bar/mybar]
; Put the bar at the bottom of the screen-default = false
;bottom = true

[module/ewmh]
type = internal/xworkspaces
label-active = " %icon% %name%  "
label-active-foreground = ${colors.foreground-alt}
label-active-background = ${colors.background-alt}
label-active-underline = ${colors.primary}
label-occupied = " %icon% %name%  "
label-occupied-underline = ${colors.secondary}
label-urgent = " %icon% %name%  "
label-urgent-foreground = ${colors.foreground}
label-urgent-background = ${colors.background}
label-urgent-underline  = ${colors.alert}
label-empty = " %icon% %name%  "
label-empty-foreground = ${colors.foreground}

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

###############################################################################
###############################################################################
############          Bruce Scott's MODULES A-Z                    ############
################################################################################
################################################################################

################################################################################

[module/cpubes1]
;https://github.com/jaagr/polybar/wiki/Module:-cpu
type = internal/cpu
; Seconds to sleep between updates
; Default: 1
interval = 1
format-foreground = ${colors.foreground}
format-background = ${colors.background}
;   
format-prefix = " "
format-prefix-foreground = #cd1f3f
;format-underline = #cd1f3f

; Available tags:
;   <label> (default)
;   <bar-load>
;   <ramp-load>
;   <ramp-coreload>
format = <label> <ramp-coreload>

format-padding = 2

; Available tokens:
;   %percentage% (default) - total cpu load
;   %percentage-cores% - load percentage for each core
;   %percentage-core[1-9]% - load percentage for specific core
label-font = 3
label = Cpu %percentage:3%%
ramp-coreload-0 = ▁
ramp-coreload-0-font = 3
ramp-coreload-0-foreground = #aaff77
ramp-coreload-1 = ▂
ramp-coreload-1-font = 3
ramp-coreload-1-foreground = #aaff77
ramp-coreload-2 = ▃
ramp-coreload-2-font = 3
ramp-coreload-2-foreground = #aaff77
ramp-coreload-3 = ▄
ramp-coreload-3-font = 3
ramp-coreload-3-foreground = #aaff77
ramp-coreload-4 = ▅
ramp-coreload-4-font = 3
ramp-coreload-4-foreground = #fba922
ramp-coreload-5 = ▆
ramp-coreload-5-font = 3
ramp-coreload-5-foreground = #fba922
ramp-coreload-6 = ▇
ramp-coreload-6-font = 3
ramp-coreload-6-foreground = #ff5555
ramp-coreload-7 = █
ramp-coreload-7-font = 3
ramp-coreload-7-foreground = #ff5555

################################################################################

[module/cpubes2]
;https://github.com/jaagr/polybar/wiki/Module:-cpu
type = internal/cpu
; Seconds to sleep between updates
; Default: 1
interval = 1
format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-prefix = "  "
format-prefix-foreground = #cd1f3f
format-underline = #cd1f3f

label-font = 3

; Available tags:
;   <label> (default)
;   <bar-load>
;   <ramp-load>
;   <ramp-coreload>
format = <label>

format-padding = 2

; Available tokens:
;   %percentage% (default) - total cpu load
;   %percentage-cores% - load percentage for each core
;   %percentage-core[1-9]% - load percentage for specific core
label = Cpu %percentage:3%%

################################################################################

[module/cpu]
type = internal/cpu
interval = 1
format-foreground = #d8dee9
format-background = #2e3440
format-underline = #A3BE8C
format-prefix = " 💻"
;format-prefix-foreground = #e5e9f0
label-font = 1
format = <label>
label = "%percentage:3%% "

################################################################################

[module/updates]
type = custom/script
exec = ~/.config/polybar/scripts/updates-Arch-OTB.sh
interval = 1000
label = " %output%"
format-foreground = #d8dee9
format-background = #2e3440
format-prefix = " 🛠️"
;format-prefix-foreground = #e5e9f0
format-underline = #5E81AC

################################################################################

[module/networkspeeddownbes]
type = internal/network
interface = enp3s0
format = DN<label>
label-connected = " %downspeed:7% "
format-connected = <label-connected>
format-connected-prefix = " ⬇️"
;format-connected-prefix-foreground = #e5e9f0
;format-connected-foreground = #d8dee9
;format-connected-background = #2e3440
;format-connected-underline = #5E81AC
format-connected-foreground = #d8dee9
format-connected-background = #2e3440
format-connected-underline = #5E81AC

################################################################################

[module/networkspeedupbes]
type = internal/network
interface = enp3s0
label-connected = " %upspeed:7% "
format-connected = <label-connected>
format-connected-prefix = " ⬆️"
;format-connected-prefix-foreground = #e5e9f0
;format-connected-foreground = #d8dee9
;format-connected-background = #2e3440
;format-connected-underline = #A3BE8C
format-connected-foreground = #d8dee9
format-connected-background = #2e3440
format-connected-underline = #A3BE8C
################################################################################

[module/networkspeedupbes2]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
;interface = wlp3s0
;interface = enp14s0
;interface = enp0s31f6
;interface = enp4s0
interface = enp3s0
label-connected = "%upspeed:7%"
format-connected = <label-connected>
format-connected-prefix = ""
format-connected-prefix-foreground = #FE522C
format-connected-foreground = ${colors.foreground}
format-connected-background = ${colors.background}
;format-connected-underline = #62FF00
;format-connected-underline = #A3BE8C

################################################################################

[module/networkspeeddownbes2]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
;interface = wlp3s0
;interface = enp14s0
;interface = enp0s31f6
;interface = enp4s0
interface = enp3s0
label-connected = "%downspeed:7%"
format-connected = <label-connected>
format-connected-prefix = ""
format-connected-prefix-foreground = #3EC13F
format-connected-foreground = ${colors.foreground}
format-connected-background = ${colors.background}
;format-connected-underline = #62FF00 old
;format-connected-underline = #5E81AC

################################################################################

[module/network-localip]
type = custom/script
exec = ~/.config/polybar/scripts/network-localip.sh
interval = 60
label = " %output% "
format-foreground = #d8dee9
format-background = #2e3440
format-prefix = " "
;format-prefix-foreground = #e5e9f0
format-underline = #A3BE8C

################################################################################

[module/temps]
type = custom/script
exec = ~/.config/polybar/scripts/bartemp.sh
interval = 30
label = " %output%"
format-foreground = #d8dee9
format-background = #2e3440
format-prefix = " CPU🌡️"
format-prefix-foreground = #e5e9f0
format-underline = #5E81AC

################################################################################

[module/memory]
type = internal/memory
interval = 1
label = "%percentage_used%% "
format = <label>
format-prefix = " 🧠 "
;format-prefix-foreground = #e5e9f0
format-underline = #5E81AC
format-foreground = #d8dee9
format-background = #2e3440

################################################################################

################################################################################
################     END Of Bruce's Moduals      ###############################
################################################################################

[module/networkspeeddown]
type = internal/network
interface = enp7s0
label-connected = " %downspeed:7% "
format-connected = <label-connected>
format-connected-prefix = " ⬇️"
;format-connected-prefix-foreground = #e5e9f0
format-connected-foreground = #d8dee9
format-connected-background = #2e3440
format-connected-underline = #5E81AC

[module/networkspeedup]
type = internal/network
interface = enp7s0
label-connected = " %upspeed:7% "
format-connected = <label-connected>
format-connected-prefix = " ⬆️"
;format-connected-prefix-foreground = #e5e9f0
format-connected-foreground = #d8dee9
format-connected-background = #2e3440
format-connected-underline = #A3BE8C

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.background-alt}
label-focused-underline= ${colors.primary}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

; Separator in between workspaces
; label-separator = |

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${module/bspwm.label-focused-background}
label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

; Separator in between workspaces
; label-separator = |

[module/date]
type = internal/date
date = %%{F#99}%d/%m/%Y%%{F-}  %%{F#fff}%I:%M %p%{F-}
date-alt = %%{F#fff}%A, %B %d, %Y  %%{F#fff}%I:%M %p%{F#666}%%{F#fba922}%S%%{F-}

[module/date2]
type = internal/date
date = %%{F#fff}%I:%M %p%{F-}  %%{F#fff}%a-%d/%b/%Y%%{F-}
date-alt = %%{F#fff}%A, %B %d, %Y  %%{F#fff}%I:%M %p%{F#666}%%{F#fba922}%S%%{F-}
format-underline = #A3BE8C
;Time: ${time %I:%M %p  |  %a,%d-%#b-%y}

[module/volume]
type = custom/script
exec = ~/.config/polybar/scripts/scripts/vol.sh
interval = 10
label = " %output%"
format-foreground = #d8dee9
format-background = #2e3440
format-prefix = " 🔉️"
;format-prefix-foreground = #e5e9f0
format-underline = #5E81AC

[module/network]
type = custom/script
exec = ~/.config/polybar/scripts/network.sh
interval = 3
label = " %output% "
format-foreground = #d8dee9
format-background = #2e3440
format-prefix = " 🔃️"
;format-prefix-foreground = #e5e9f0
format-underline = #5E81AC

[module/batt]
type = custom/script
exec = ~/bin/batt.sh
interval = 30
label = " %output%"
format-foreground = #d8dee9
format-background = #2e3440
format-prefix = "🔋️"
;format-prefix-foreground = #e5e9f0
format-underline = #5E81AC

[module/sept]
type = custom/text
content = |
content-padding = 0
;content-margin = 0
content-foreground = ${color.foreground}
content-background =  ${color.background}
;content-underline = ${color.db-cyan}

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
;pseudo-transparency = false

;[global/wm]
;margin-top = 5
;margin-bottom = 5

; vim:ft=dosini
