#!/bin/bash
#------------------------------------------------------------------------------
# Project Name      - Subs_polybar.sh for BEW Ytube channel
# Started On        - 01 Feb 7:44 p.m. EST 2023
# Last Change       - 01 Feb 8:15 a.m. EST 2023
# Author E-Mail     - bescott9944@gmail.com
# Author GitHub     - https://github.com/bescott9944
# Auther Gitlab     - https://gitlab.com/bescott9944
#------------------------------------------------------------------------------

# Looking for the file
if [ -f  "$HOME/Documents/Master-Docs/subs_bew.txt" ]; then
    bew_subs=$(cat "$HOME/Documents/Master-Docs/subs_bew.txt")
        echo $bew_subs

#notify-send -t 3000 "NOPE!" "$bookmark already exists in $file"
else
    echo "File subs.txt Not Found!" && \
    notify-send -t 9000 "ERROR!" "File subs_bew.txt Not Foumd!"
fi
