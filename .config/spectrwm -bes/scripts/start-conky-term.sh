#!/bin/bash
# Starting Conky and Terminal
sleep 6  &> /dev/null
conky -c /home/bruce/.config/spectrwm/conkys/UFP-Spectrwm-Arch-Main-Net-lua-Fixed &
sleep 7
conky -c /home/bruce/.config/spectrwm/conkys/UFP-Spectrwm-Net-Port-Connection-lua-Fixed &
sleep 8
terminator