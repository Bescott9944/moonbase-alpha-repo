#!/bin/bash
#------------------------------------------------------------------------------
# Project Name      - Subs_BWOS_polybar.sh for BWOS Ytube channel
# Started On        - 21 March 7:44 p.m. EST 2023
# Last Change       - 04 April 8:15 a.m. EST 2023
# Author E-Mail     - bescott9944@gmail.com
# Author GitHub     - https://github.com/bescott9944
# Auther Gitlab     - https://gitlab.com/bescott9944
#------------------------------------------------------------------------------

# Looking for the file
if [ -f  "$HOME/Documents/Master-Docs/subs_bwos.txt" ]; then
    bwos_subs=$(cat "$HOME/Documents/Master-Docs/subs_bwos.txt")
        echo $bwos_subs

#notify-send -t 3000 "NOPE!" "$bookmark already exists in $file"
else
    echo "File subs.txt Not Found!" && \
    notify-send -t 9000 "ERROR!" "File subs.txt Not Foumd!"
fi
