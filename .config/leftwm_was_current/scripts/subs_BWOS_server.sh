#!/bin/bash
#------------------------------------------------------------------------------
# Project Name      - Subs_BWOS_polybar.sh for BWOS Ytube channel
# Started On        - 21 March 7:44 p.m. EST 2023
# Last Change       - 04 April 8:15 a.m. EST 2023
# Author E-Mail     - bescott9944@gmail.com
# Author GitHub     - https://github.com/bescott9944
# Auther Gitlab     - https://gitlab.com/bescott9944
#------------------------------------------------------------------------------
set -e
# link to my videos page on youtube
#url="https://www.youtube.com/c/linuxdabbler/videos"
url="https://www.youtube.com/@BrucesWorldofStuff/videos"

# Check to see if output file is there. If not make it!
if [ -f /N-1-MD-500-Sata/Master-Documents/subs_bwos.txt ]; then
    echo "All Good, subs_bew.txt is here!"
else
    touch /N-1-MD-500-Sata/Master-Documents/subs_bwos.txt
fi

# downloaded file location
if [ -d /N-1-MD-500-Sata/Master-Documents ];then
    file="/N-1-MD-500-Sata/Master-Documents/subs_bwos.txt"

else
    echo "Error! File not updated or created. ???"
fi

# Testing exit codes
if [ $? -eq 0 ]; then
    echo "It Exited with "0"!"
else
    echo " it failed"
    exit $?
fi

# get subscribers from downloaded file
getinfo="$(curl $url | sed 60q | tr , '\n' | grep subscribers | tail -n1 | cut -d':' -f 2 | sed 's|\}||; s|"||g' > $file)"

curl=$?
if [ $curl != 0 ]; then
    echo "Curl returm error 23 and exited with: $curl"
    exit $curl
fi

echo "The File subs_bwos.txt Was Updated!"
# what did the script exit with?
echo $?
