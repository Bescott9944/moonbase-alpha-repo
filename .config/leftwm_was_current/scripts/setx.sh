#!/bin/bash
# Using Xset Setting to disable the screen blanking in a window manager.
echo "Setting Xset in 5 sec."
sleep 4
xset -dpms s noblank s off
echo "All Done!"
exit
