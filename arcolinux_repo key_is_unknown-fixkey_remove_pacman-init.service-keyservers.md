
## arcolinux_repo key is unknown - fixkey - remove pacman-init.service - keyservers
### 10-10-22

If you get a bunch of Arch keys not working it may be that pacman-init.service is installed using a old USB or ISO that still has pacman-init as part of the key service. The Arch folks remved this in the arch.iso and the arch install. So when you install using a older install script you may get the error that some keys are not found.
To fix this follow the steps below

- sudo systemctl disable pacman-init.service
- reboot
- sudo rm /etc/systemd/system/pacman-init.service
- reboot again
- run a update and you should not have anymore isses.
- Things ToDo:
- add keys servers to your /etc/pacman.d/gnupg/gpg.conf
- Ubuntu one seems to have a great success rate

See AecoLinux's video: https://www.youtube.com/watch?v=ROLVePGlKuU
- -Bruce