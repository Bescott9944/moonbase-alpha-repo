# Change-log.md
      _                                           
     /  |_   _. ._   _   _  |  _   _    ._ _   _| 
     \_ | | (_| | | (_| (/_ | (_) (_| o | | | (_| 
                     _|            _|             


- Lead Developer: Bruce E. Scott
- Script Creator: Bruce E. Scott
- GitLab: https://gitlab.com/bescott9944
- Forums: https://forum.endeavouros.com/bescott9944
- Licensed under the GNU and MIT respectively


####   <<<---================}[ Moonbase-Alpha-Repo & Build-Files ]{================--->>>

### -----------------------------------------------------

#### Current Version: None Modified by Bruce Scott 05/23/2022
* Project Name  : My LeftWm--SpectrWm Dot Config  & Build-Files
* Description   : All my Scripts and files needed to reinstall SpectrWM & LeftWm
* Author        : Bruce E. Scott, May 23 2022
* Started On    : 23 May 2022
* Last Change   : 23 May 01:15 a.m. EST 2022
* Author Email : bescott9944@gmail.com
* Author GitHub : https://github.com/bescott9944
* Author Gitlab : https://gitlab.com/bescott9944
* Linux Forms   : I can be reached at https://forum.endeavouros.com/bescott9944 (bescott9944)
### -----------------------------------------------------


#### May 23th 2022
- Setup the Repo to upload all the scripts for my builds of SpectrWm
- And LeftWm Window managers. This includes all of my /bin,
/.config, /.conky, and other misc files needed.

### -----------------------------------------------------

#### Change log. 05/24/2022
- Added my updated Readme.MD. Added some file too.
- This is also kinda testing everything too. -Bruce
### -----------------------------------------------------

#### Change log. 05/25/2022
- Well we spent several hours uploading all the files.
- Sure is lots of fun that is for sure! All my file are there for you to use
- if you want. Just remember they will have to be adjusted to be used on your
- system. Use at your OWN Risk. You have been WARNED!!!
### -----------------------------------------------------

#### Change log. 06/04/2022
- Well I was updating some files on the Repo and I screwed it up so bad I
- Deleted the Repo because the more I tried to fix it the worse I made it!
- Git and I are not the best of friends as I know almost nothing about it.
- So I spent a few hours today totally redoing everything and removing the "master-back-date"
- references as I do have the backup in 2 local places and a backup drive.
- So I am glad the issue happened and I did have a chance to fix the way the Repo
- is laid out... -Bruce
### -----------------------------------------------------

#### Change log. 10/08/2022
## IMPORTANT, LeftWm has changed to .RON From .TOML

- **IMPORTANT NOTE: LeftWM has changed the config language from `TOML` to `RON` with the `0.4.0` release. Please use `leftwm-check --migrate-toml-to-ron` to migrate your config and visit the [wiki](https://github.com/leftwm/leftwm/wiki) for more info.**

- ** Also Note: LeftWM's file format has also changed in the 'config.ron' and all the 'theme's' configs have changed too. The 'theme.ron' format has also changed...
- The format for the 'config.ron' file and the 'theme changes' can be seen in a video by Erik Dubois. See these videos for how to do this.
- https://www.youtube.com/watch?v=CMqGFlpvh9M and https://www.youtube.com/watch?v=1uOCAo4sONE **
- Do the change in programming I had to change everything from .Toml to .Ron...
- Also had to make lot's of changes to the theme's do to the nrw format too... -Bruce

### -----------------------------------------------------

### -----------------------------------------------------

### -----------------------------------------------------

### -----------------------------------------------------
