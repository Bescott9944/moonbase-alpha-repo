## Welcome to my Repo for LeftWM & SpectrWM Window Manager.

### (Use At Your Own Risk)

![Screenshot of LeftWM in action](screenshots/leftwm_desktop_thumb.png)

![Screenshot of LeftWM in action](screenshots/leftwm_term_thumb.png)

The purpose of this Repo is for reinstalling my system file and Window Manager files after a reinstall or building a new system.
Here you will find my Dot Files and Scripts I use for my LeftWM & SpectrWM setup that I use on my EndeavourOS Arch install...
I also use these on my Linux Mint, my BTW Arch, MX 19 and others when I get a chance... Note, If you use these you may have to install
files or dependencies that I use and you don't have.

Some of these files were borrowed from OTB, DT, Linux Dabbler and other sources. I don't remember where I got them all
but here they are! I know some of the script might not be worded right and I am not a programmer by any-means but they work for me.
Edit them as you wish... Make then work for you!
I have included all my ~/bin files for public use. Also my Polybar, Rofi, and other files, my config file, Themes build files.. Enjoy!

Erik Dubois Over at ArcoLinux project : https://arcolinux.info has made it possible to add his repos to any Arch system and install
his new"Archlinux-Tweek-Tool" to install his builds of lots of desktops and window managers.
Visit Erik Dubois Youtube channel: https://www.youtube.com/erikdubois.
Also there Github : https://github.com/arcolinux, https://arcolinux.com for tons of information and videos!
Watch the videos and there website and GitHub for tons of information

## IMPORTANT, LeftWm has changed to .RON From .TOML

**IMPORTANT NOTE: LeftWM has changed the config language from `TOML` to `RON` with the `0.4.0` release. Please use `leftwm-check --migrate-toml-to-ron` to migrate your config and visit the [wiki](https://github.com/leftwm/leftwm/wiki) for more info.**

**Also Note: LeftWM's file format has also changed in the 'config.ron' and all the 'theme's' configs have changed too. The 'theme.ron' format has also changed...
The format for the 'config.ron' file and the 'theme changes' can be seen in a video by Erik Dubois. See these videos for how to do this.
https://www.youtube.com/watch?v=CMqGFlpvh9M and https://www.youtube.com/watch?v=1uOCAo4sONE .**

## [0.4.0]
### Fixed
- Fixed again a lot of small papercuts
- Command parity between keybinds and external aka `leftwm-command` commands
- Improved window snapping
- Improved behaviour of floating windows
- `dock` windows not recieving `click` after some window was `fullscreen`
### Added
- Commands `AttachScratchpad`, `ReleaseScratchpad`, `NextScratchpadWindow`, `PrevScratchpadWindow`
- Commands `MoveWindowToNextTag`, `MoveWindowToPreviousTag`
- Window rules by `WINDOW_CLASS` or `WINDOW_TITLE`
- `test-full` to `Makefile` using padantic clippy as well
- Introduced `lefthk` as default daemon to handle keybinds
- Changed config language from `TOML` to `RON`
- Journald logging is default for AUR packages and when building from source
- Tags support `urgent` flag
- `sloppy_mouse_follows_focus`
- `disable_window_snap`
`### Minimum Supported Rust Version
- The currently supported MSRV is 1.59.0

One thing I found useful is to search through the LeftWm (Issues) Bug Reports. I found some good information that was and is not in the Wiki yet.
Go over to: https://github.com/leftwm/leftwm for information.
So make sure you check there. Also there's lots of good help on the Github site...
Also you can and should use YAY (or you fav AUR tool) and download the leftwm-git version. I was told
by the Devs. This is updated with all the patches often. Give it a try! I will...

I have found some things that were not in the LeftWM Wiki...
I got them from the (Issues) Bug Reports and they fixed some of my issues.
I found this below Bindings in a Report. Then later found in the Wiki, I was looking to hard.
```
(command: FocusNextTag, value: "", modifier: ["modkey"], key: "Right"),
(command: FocusPreviousTag, value: "", modifier: ["modkey"], key: "Left"),
```
With these I can use the arrow keys and it works just like in SpectrWM where you hold down the Modkey
and use the arrow keys to move between the tags!
This was a must for me. If you like to use the Vim keys just change the "KEY = "J"" or to what ever you want.

## OLD NEWS... 2021... Read on!

Now I had some screen resolution issue and I had to file a (Issues) Bug Report.
The Dev, an 2 threes jumped right in on the Bug Report and had me fixed within few days of texting back and forth.

 I got LeftWm all sorted.. Took a bit of work and a bunch of chatting on the Github (Issues)Bug Report but thanks for the Great folks over there we got things working...
The Polybar was easy. I figured that one out. I looked at other Themes that had the bar on the bottom and found out how they did that
There is a setting you change and it is on the bottom... Like this
```
To Put the polybar at the bottom of the screen. The default is bottom = false
bottom = true    <--- Set it to this for the bottom bar.
```

The desktop was a 4 day message back and forth / testing and the great help I got was fantastic!
LeftWm desktop would not go into 1920x1080 and stayed stuck in 1360x768. Arch's default boot this monitor / Tv at 1360x768 and EndeavourOS changes it to 1920x1080 on booting into Cinnamon.
Logging into LeftWm you don't have that and there are things one needs get the resolution set right. Most folks have a xorg.conf I guess, that deals with that but AMD does not have any setup tool for Linux so ya have to do things manually.
When I set the display with xrandr LeftWm would change but the desktop is all squished into the upper left of the screen.

In the end to fix my issue after 4 days. They had me created a ".xprofile" and added the xrandr commands in there...
This is the commands that I used for my Monitor / TV...
```
xrandr --output VGA-0 --off --output DVI-0 --off --output HDMI-0 --primary --mode 1920x1080 --pos 0x0 --rotate normal
```

Now LeftWm boots into 1920x1080 and all is right as rain! :-D
As a added Bonus, SpectrWm uses .xprofile too and the resolution was fixed there too. That meant I could do away with 2 scripts that I was using for Spectrwm to get the resolution set, so Yall.. Things are great!
I hope this may help anyone else that has these issues... :-)

So that is some of the things I had fun with and yes it was a adventure! Lol

Please install all the Dependencies and programs, Themes. Most of all follow the instruction on the Themes to make the Links to the folders.
That is it! Have fun!
Thanks LeftWm Devs for such a cool Window Manger!




