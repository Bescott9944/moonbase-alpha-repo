#!/bin/bash

journalchk(){
  journalctl -p 3 -xb > /tmp/journalchk
  nano /tmp/journalchk
  clear
  echo -e "\n"
  rm /tmp/journalchk

}

#
yaysysupdate () {
  yay -Syu
  echo -e "\n"
  echo "Yay Full system / AUR upgrade finished"
  sleep 2
  clear
}
#
click_one(){
   yad --center --text="Clicked the one"
}

click_two(){
   yad --center --text="Clicked the two"
}

export -f click_one click_two journalchk yaysysupdate

yad \
    --title "My Title" \
    --center --text="Click a button to see what happens" \
    --button="One":"bash -c click_one" \
    --button="Check journal logs":"bash -c journalchk" \
    --button="Run Yay system update":"bash -c yaysysupdate" \
    --button="Two":"bash -c click_two" \
    --button="Date":"date" \
    --button="Exit":0


echo $?


function on_click () {
echo clicked
}
export -f on_click

yad  --title "My Title" --form --field "Button:BTN" "bash -c on_click"

